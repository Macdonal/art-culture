<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>{{$album->name}}</title>
    <!-- Latest compiled and minified CSS -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/css/bootstrap.min.css" rel="stylesheet">

      <style>
          body {
              padding-top: 50px;
          }
          .starter-template {
              padding: 40px 15px;
              text-align: center;
          }
      </style>
  </head>
  <body>
  <div class="nav-collapse collapse" style="float: right;">
      <div class="navbar navbar-inverse navbar-fixed-top" style="background-color: #f9f9f9;">
          <div class="container" style="background-color: #f9f9f9;">
              <button type="button" class="navbar-toggle"data-toggle="collapse" data-target=".nav-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>

              <h3 style="float: left">Google Arts & Culture</h3>
              {{--        <a class="navbar-brand" href="/" style="color: #000000;float: left;">Home</a>--}}

              <ul class="nav navbar-nav"style="float: right">
                  <a class="navbar-brand" href="/" style="color: #000000;">Home</a>
                  <li><a href="{{URL::route('create_album_form')}}" style="color: #000000;">Create</a></li>
                  <li><a href="#" style="color: #000000;">Explore</a></li>
                  <li><a href="#" style="color: #000000;">Nearby</a></li>
                  <li><a href="#" style="color: #000000;">Favorites</a></li>
                  <svg class="bi bi-grid-3x3-gap-fill" width="1.5em" height="3.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path d="M1 2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H2a1 1 0 01-1-1V2zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H7a1 1 0 01-1-1V2zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1h-2a1 1 0 01-1-1V2zM1 7a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H2a1 1 0 01-1-1V7zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H7a1 1 0 01-1-1V7zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1h-2a1 1 0 01-1-1V7zM1 12a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H2a1 1 0 01-1-1v-2zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H7a1 1 0 01-1-1v-2zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1h-2a1 1 0 01-1-1v-2z"/>
                  </svg>
              </ul>
          </div>
      </div>
  </div>

    <div class="container">

        <div class="media">
            <p style="font-size: 50px;margin-top: 30px;">{{$album->name}}</p>
            <p style="font-size: 20px;">{{$album->description}}<p>
            <img class="media-object pull-left"alt="{{$album->name}}" src="/albums/{{$album->cover_image}}" style="width: 100%;margin-top: 10px;">
            <div class="row">

                @foreach($album->Photos as $photo)


                    <div class="col-lg-3"style="width: 400px;">

                        <div class="thumbnail" style="max-height: 350px;min-height: 500px;">
                            <img alt="{{$album->name}}" src="/albums/{{$photo->image}}">
                            <div class="caption">
                                <p>{{$photo->description}}</p>
                                <p>Created date:  {{ date("d F Y",strtotime($photo->created_at)) }}at {{ date("g:ha",strtotime($photo->created_at)) }}</p>

                                <p>Move image to another Album :</p>
                                <form name="movephoto" method="POST"action="{{URL::route('move_image')}}">
                                    {{ csrf_field() }}
                                        <button class="btn btn-secondary">
                                            <select name="new_album">
                                                @foreach($albums as $others)
                                                    <option value="{{$others->id}}">{{$others->name}}</option>
                                                @endforeach
                                            </select>
                                        </button>


                                    <input type="hidden" name="photo"value="{{$photo->id}}" />
                                    <button type="submit" class="btn btn-warning btn-small" onclick="return confirm('Are you sure?')" style="float: right">Move Image</button>
                                    <a href="{{URL::route('delete_image',array('id'=>$photo->id))}}" onclick="returnconfirm('Are you sure?')"><button type="button"class="btn btn-light btn-small" style="float:right;">Delete Image</button></a>

                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="media-body">
    </div>
            <div class="starter-template">
{{--                <h2 class="media-heading" style="width:100%;font-size: 20px; padding-top: 100px;">Album Name</h2>--}}

                <div class="media">
{{--                    <h2 class="media-heading" style="font-size: 20px;">Album Description</h2>--}}

                        <a href="{{route('add_image',array('id'=>$album->id))}}"><button type="button"class="btn btn-light">Add New Image to Album</button></a>
                        <a href="{{route('delete_album',array('id'=>$album->id))}}" onclick="return confirm('Are yousure?')"><button type="button" class="btn btn-light">Delete</button></a>
                </div>
            </div>
    </div>


    </div>
    <!-- Latest compiled and minified JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script-->
  </body>
</html>
