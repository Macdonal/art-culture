<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Awesome Albums</title>
    <!-- Latest compiled and minified CSS -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/css/bootstrap.min.css" rel="stylesheet">

    <!-- Latest compiled and minified JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/js/bootstrap.min.js"></script>
    <style>
      body {
        padding-top: 50px;
      }
      .starter-template {
        padding: 40px 15px;
      text-align: center;
      }
    </style>
  </head>
  <body>
  <div class="nav-collapse collapse" style="float: right;">
  <div class="navbar navbar-inverse navbar-fixed-top" style="background-color: #f9f9f9;">
    <div class="container" style="background-color: #f9f9f9;">
      <button type="button" class="navbar-toggle"data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

<h3 style="float: left">Google Arts & Culture</h3>


        <ul class="nav navbar-nav"style="float: right">
            <a class="navbar-brand" href="/" style="color: #000000;">Home</a>
          <li><a href="<?php echo e(URL::route('create_album_form')); ?>" style="color: #000000;">Create</a></li>
          <li><a href="#" style="color: #000000;">Explore</a></li>
          <li><a href="#" style="color: #000000;">Nearby</a></li>
          <li><a href="#" style="color: #000000;">Favorites</a></li>
          <svg class="bi bi-grid-3x3-gap-fill" width="1.5em" height="3.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path d="M1 2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H2a1 1 0 01-1-1V2zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H7a1 1 0 01-1-1V2zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1h-2a1 1 0 01-1-1V2zM1 7a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H2a1 1 0 01-1-1V7zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H7a1 1 0 01-1-1V7zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1h-2a1 1 0 01-1-1V7zM1 12a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H2a1 1 0 01-1-1v-2zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H7a1 1 0 01-1-1v-2zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1h-2a1 1 0 01-1-1v-2z"/>
          </svg>
        </ul>
      </div>
    </div>
  </div>



      <div class="container" >



        <div class="row">
          <?php $__currentLoopData = $albums; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $album): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-lg-3"style="min-width: 350px;margin-top: 20px;">
              <div class="thumbnail" style="min-height: 200px;">
                <img alt="<?php echo e($album->name); ?>" src="/albums/<?php echo e($album->cover_image); ?>">
                <div class="caption">
                  <h3><?php echo e($album->name); ?></h3>
                  <p><?php echo e($album->description); ?></p>
                  <p><?php echo e(count($album->Photos)); ?> image(s).</p>
                  <p>Created date:  <?php echo e(date("d F Y",strtotime($album->created_at))); ?> at <?php echo e(date("g:ha",strtotime($album->created_at))); ?></p>
                  <p><a href="<?php echo e(route('show_album', ['id'=>$album->id])); ?>" class="btn btn-big btn-default">Show Gallery</a></p>
                </div>
              </div>
            </div>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>

      </div><!-- /.container -->


  </body>
</html>
