<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title><?php echo e($album->name); ?></title>
    <!-- Latest compiled and minified CSS -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/css/bootstrap.min.css" rel="stylesheet">

      <style>
          body {
              padding-top: 50px;
          }
          .starter-template {
              padding: 40px 15px;
              text-align: center;
          }
      </style>
  </head>
  <body>
  <div class="nav-collapse collapse" style="float: right;">
      <div class="navbar navbar-inverse navbar-fixed-top" style="background-color: #f9f9f9;">
          <div class="container" style="background-color: #f9f9f9;">
              <button type="button" class="navbar-toggle"data-toggle="collapse" data-target=".nav-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>

              <h3 style="float: left">Google Arts & Culture</h3>
              

              <ul class="nav navbar-nav"style="float: right">
                  <a class="navbar-brand" href="/" style="color: #000000;">Home</a>
                  <li><a href="<?php echo e(URL::route('create_album_form')); ?>" style="color: #000000;">Create</a></li>
                  <li><a href="#" style="color: #000000;">Explore</a></li>
                  <li><a href="#" style="color: #000000;">Nearby</a></li>
                  <li><a href="#" style="color: #000000;">Favorites</a></li>
                  <svg class="bi bi-grid-3x3-gap-fill" width="1.5em" height="3.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path d="M1 2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H2a1 1 0 01-1-1V2zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H7a1 1 0 01-1-1V2zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1h-2a1 1 0 01-1-1V2zM1 7a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H2a1 1 0 01-1-1V7zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H7a1 1 0 01-1-1V7zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1h-2a1 1 0 01-1-1V7zM1 12a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H2a1 1 0 01-1-1v-2zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1H7a1 1 0 01-1-1v-2zm5 0a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 01-1 1h-2a1 1 0 01-1-1v-2z"/>
                  </svg>
              </ul>
          </div>
      </div>
  </div>

    <div class="container">

        <div class="media">
            <p style="font-size: 50px;margin-top: 30px;"><?php echo e($album->name); ?></p>
            <p style="font-size: 20px;"><?php echo e($album->description); ?><p>
            <img class="media-object pull-left"alt="<?php echo e($album->name); ?>" src="/albums/<?php echo e($album->cover_image); ?>" style="width: 100%;margin-top: 10px;">
            <div class="row">

                <?php $__currentLoopData = $album->Photos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $photo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


                    <div class="col-lg-3"style="width: 400px;">

                        <div class="thumbnail" style="max-height: 350px;min-height: 500px;">
                            <img alt="<?php echo e($album->name); ?>" src="/albums/<?php echo e($photo->image); ?>">
                            <div class="caption">
                                <p><?php echo e($photo->description); ?></p>
                                <p>Created date:  <?php echo e(date("d F Y",strtotime($photo->created_at))); ?>at <?php echo e(date("g:ha",strtotime($photo->created_at))); ?></p>

                                <p>Move image to another Album :</p>
                                <form name="movephoto" method="POST"action="<?php echo e(URL::route('move_image')); ?>">
                                    <?php echo e(csrf_field()); ?>

                                        <button class="btn btn-secondary">
                                            <select name="new_album">
                                                <?php $__currentLoopData = $albums; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $others): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($others->id); ?>"><?php echo e($others->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </button>


                                    <input type="hidden" name="photo"value="<?php echo e($photo->id); ?>" />
                                    <button type="submit" class="btn btn-warning btn-small" onclick="return confirm('Are you sure?')" style="float: right">Move Image</button>
                                    <a href="<?php echo e(URL::route('delete_image',array('id'=>$photo->id))); ?>" onclick="returnconfirm('Are you sure?')"><button type="button"class="btn btn-light btn-small" style="float:right;">Delete Image</button></a>

                                </form>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <div class="media-body">
    </div>
            <div class="starter-template">


                <div class="media">


                        <a href="<?php echo e(route('add_image',array('id'=>$album->id))); ?>"><button type="button"class="btn btn-light">Add New Image to Album</button></a>
                        <a href="<?php echo e(route('delete_album',array('id'=>$album->id))); ?>" onclick="return confirm('Are yousure?')"><button type="button" class="btn btn-light">Delete</button></a>
                </div>
            </div>
    </div>


    </div>
    <!-- Latest compiled and minified JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script-->
  </body>
</html>
